<?php

namespace Drupal\twiggy\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * @Filter(
 *   id = "filter_twiggy",
 *   title = @Translation("Twiggy Filter"),
 *   description = @Translation("Allows for the user of Twig in our content!"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class FilterTwiggy extends FilterBase {
  public function process($text, $langcode) {
    $twig_service = \Drupal::service('twig');
    return new FilterProcessResult((string) $twig_service->renderInline($text, ['langcode' => $langcode]));
  }
}
